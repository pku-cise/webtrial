// import store from '.store';

/* 发送请求，等待回应（同步）
 * 如果收到返回回应则返回该回应，否则返回空串
 * 
 * Arguments:
 * self -- vue实例(this)，小心this指代问题
 * method -- http请求(post/get/..)
 * url -- url
 * data -- data
 * async -- async
 * 
 * Return -- response，返回空表示没有收到response
 */
async function axios_request(self, method, url, data, async = true) {
    self.axios.defaults.baseURL = ''
    
    // 添加user_name到发送的data中
    data['user_name'] = self.$store.state.user_name
    
    try {
        let res = await self.axios({
            method,
            url,
            async,
            data,
        })

        return res
    }
    catch (error) {
        //alert('网络错误')
        self.$message({
            type: 'error',
            message: '网络错误'
        })

        console.log(error)
        return ''
    }
}

async function getColdBootEntityRecommendData(self) {
    const that = self

    let res = await axios_request(that, 'post', '/api', {
        operation: 'getGlobalEntityRecommend',
        user_id: that.$store.state.Authorization,
        project: that.$store.state.project_name
    })

    // console.log('get cold boot data')
    // console.log(res)

    if (res != '') {
        if (!res.data.error && !that.$store.state.getColdBootEntityFlag) { // 获取冷启动实体推荐数据成功
            // 设置已发出冷启动实体推荐数据请求
            that.$store.state.getColdBootEntityFlag = true
            that.$store.state.coldBootEntityRecommendData = res.data.entities
            
            for (let nodeType of that.$store.state.nodeTypeList)
                if (that.$store.state.coldBootEntityRecommendData[String(nodeType.global_id)] == undefined)
                    that.$store.state.coldBootEntityRecommendData[String(nodeType.global_id)] = []
        }
    }
}

async function getGlobalRecommendInfo(self) {
    const that = self

    let res = await axios_request(that, 'post', '/api', {
        operation: 'getGlobalRecommend',
        user_id: that.$store.state.Authorization,
        project: that.$store.state.project_name
    })

    console.log('get global recommend info')
    console.log(res)

    if (res != '') {
        if (!res.data.error && !that.$store.state.getGlobalRecommendInfoFlag) {
            // 已请求推荐信息
            that.$store.state.getGlobalRecommendInfoFlag = true
            that.$store.state.globalRecommendInfo = res.data.insts
        }
    }
}

export {
    axios_request,
    getColdBootEntityRecommendData,
    getGlobalRecommendInfo
}