/* 标识复杂类型的常数 */
const SimpleType = 0        // simple
const IntersectionType = 1  // intersection
const UnionType = 2         // union

/* 交和并的符号 */
const IntersectionOpt = '∩'
const UnionOpt = '∪'

export {
    SimpleType,
    IntersectionType,
    UnionType,
    IntersectionOpt,
    UnionOpt,
}