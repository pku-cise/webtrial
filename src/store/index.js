import { createStore } from 'vuex'
import {SimpleType, IntersectionType, UnionType, IntersectionOpt, UnionOpt} from '../util/const_complex_type.js'

// 用来parse类型的name（处理交和并），现在只处理了一层
function getTypeName(entityTypeList, type) {
  let tmp = type
  try {
    tmp = JSON.parse(type);
  }
  catch (err) {
    tmp = type
  }

  if (typeof(tmp) == 'string') {
    return tmp
  }
  else {
    let str = ''
    let opt = ''
    if (tmp["operator"] == "or") opt = UnionOpt
      else opt = IntersectionOpt
    
    for (let i = 0; i < tmp.ids.length; i++) {
      if (i != 0) str = str + opt
      let name = entityTypeList.find(item => item.id == tmp.ids[i]).name
      // 在只有“一层”的情况下，这个写法是没有问题的
      str = str + name
    }

    return str
  }
}

export default createStore({
  state: {
    // 存储token（暂时是user_id）
    Authorization: localStorage.getItem('Authorization') ? localStorage.getItem('Authorization') : '',

    // project_name
    project_name : '',
    // user_name
    user_name : '',
    // kg list
    KGList: [{'name': 'test_project', 'project_name': 'test_project'}],

    //测试用
    testValue : 3,

    // 暂时用来村服务器返回的的graph，用个对象表示{project_name : graph}
    project_graph : {},

    //demo展示用色表（共三种颜色，node两种颜色，link就用一种颜色），要改成自动化点的
    colorList : JSON.parse(localStorage.getItem('colorList')) || [
      //{'color': '#409EFF'},
      { 'color': '#85c1fd' },
      { 'color': '#FFD700' },
      { 'color': '#9ACD32' },
      { 'color': '#CD5C5C' },
      { 'color': '#FFA500' },
      { 'color': '#66CDAA' },
      { 'color': '#CDC8B1' },
    ],

    // 是否曾经获取过冷启动实体推荐
    coldBootEntityRecommendFlag : false,
    // 冷启动实体推荐数据
    coldBootEntityRecommend : {},
    // 是否曾经获取推荐信息（哪些实体/关系具有推荐信息）
    globalRecommendInfoFlag : false,
    // 推荐信息
    globalRecommendInfo : {},
    // 局部图谱推荐信息
    localRecommendInfo: {},

    // 记录同名实体/关系的数目
    nodesDataMap : {},
    linksDataMap : {},

    nodeTypeList : JSON.parse(localStorage.getItem('nodeTypeList')) || [],

    linkTypeList : JSON.parse(localStorage.getItem('linkTypeList')) || [],

    roleTypeList : JSON.parse(localStorage.getItem('roleTypeList')) || [],

    nodesData : JSON.parse(localStorage.getItem('nodesData')) || [],

    linksData : JSON.parse(localStorage.getItem('linksData')) || [],

    symbolData: JSON.parse(localStorage.getItem('symbolData')) || [],

    constraintData: JSON.parse(localStorage.getItem('constraintData')) || [],
  },

  mutations: {
    // 修改token，并将token存入localStorage
    changeLogin (state, user) {
      state.Authorization = user.Authorization;

      // 具体localStorage的用法之后再说～ ###
      localStorage.setItem('Authorization', user.Authorization);
    },

    storeUserName (state, data) {
      state.user_name = data
    },

    storeKGList (state, data) {
      state.KGList = data
    },

    logout (state) {
      state.Authorization = ''

      localStorage.removeItem('Authorization')
    },

    changeTestValue (state, value) {
      state.testValue = value
    },

    /************************************/
    /*** 修改的时候没有修改project_graph ***/
    /************************************/

    //data格式同文档
    addEntityType (state, data) {
      // properties
      let properties = []
      if (data.properties != undefined) {
        for (let name in data.properties) {
          let type_name = data.properties[name].type_name
          let type_id = data.properties[name].type_id
          let type = state.symbolData[String(type_id)].type

          properties.push({
            'property_name': name,
            'type_name': type_name,
            'type_id': type_id,
            'type': type
          })
        }
      }

      if (data.isComplex == SimpleType) {
        state.project_graph[state.project_name].entity_types[String(data.global_id)] = {
          'name': data.entity_type,
          'properties': data.properties
        }

        state.nodeTypeList.push({
          'global_id': data.global_id,
          'name': data.entity_type,
          'type': data.entity_type,
          'properties': properties,
          'isComplex': SimpleType
        })
      }
      else {
        let type_struct = {
          'operator': "and",
          'ids': data.type_ids
        }
        if (data.isComplex == IntersectionType) type_struct.operator = "and"
          else type_struct.operator = "or"
        
        let type_name = getTypeName(state.project_graph[state.project_name].entity_types, type_struct)

        state.project_graph[state.project_name].entity_types[String(data.global_id)] = {
          'name': type_name
        }

        state.nodeTypeList.push({
          'global_id': data.global_id,
          'name': type_name,
          'type': type_name,
          'properties': properties,
          'isComplex': data.isComplex,
          'struct': type_struct
        })
      }
    },

    addRelationType (state, data) {
      // 处理新增的roleType
      for (let roleType of data.role_types) {
        if (state.roleTypeList.find(i => i.global_id == roleType.id) == undefined) {
          let id = roleType.id
          let type_ids = roleType.type_ids
          let name = ''

          // get name
          for (let ids of type_ids) {
            let tmp = state.nodeTypeList.find(i => i.global_id == ids)
            if (tmp == undefined)
              tmp = state.linkTypeList.find(i => i.global_id == ids)
            if (name != '') name += UnionOpt
            name += tmp.name
          }

          state.roleTypeList.push({
            'global_id': id,
            'type_ids': type_ids,
            'name': name,
            'type': name,
          })
        }
      }

      // 处理roles
      let l = []
      for (let r in data.roles) {
        let type_id = data.roles[r].type_id

        //let entity_type = state.nodeTypeList.find(item => {
        //    return type_id == item.global_id
        //})
        let entity_type = state.nodeTypeList.find(i => type_id == i.global_id)
        if (entity_type == undefined)
          entity_type = state.linkTypeList.find(i => type_id == i.global_id)
        if (entity_type == undefined)
          entity_type = state.roleTypeList.find(i => type_id == i.global_id)

        let type_name = entity_type.name
        l.push({
          'global_id': type_id,
          'name': r, // 扮演的角色名
          // 还没有处理数目！！！！！！
          'type': type_name, // 角色的类型
          'roles': data.roles[r] // 存了所有东西
        })
      }

      // 处理properties
      let properties = []
      for (let name in data.properties) {
        let type_name = data.properties[name].type_name
        let type_id = data.properties[name].type_id
        let type = state.symbolData[String(type_id)].type

        properties.push({
          'property_name': name,
          'type_name': type_name,
          'type_id': type_id,
          'type': type
        })
      }

      state.linkTypeList.push({
        'global_id': data.global_id,
        'name': data.relation_type,
        'type': data.relation_type,
        'properties': properties,
        'entity': l
      })

      state.project_graph[state.project_name].relation_types[String(data.global_id)] = {
        'name': data.relation_type,
        'properties' : data.properties,
        'roles': data.roles
      }
    },

    addSymbolType (state, data) {
      let symbol_type_id = data['symbol_type_id']
      let newSymbolType = {
        'name': data['symbol_name'],
        'type': data['symbol_type']
      }

      if (data['symbol_type'] == 'Enum') {
        newSymbolType['enum_values'] = data['enum_values']
      }
      else if (data['symbol_type'] == 'Compound') {
        newSymbolType['child_symbols'] = data['child_symbols']
      }

      state.symbolData[symbol_type_id] = newSymbolType
    },

    changeEntityName (state, data) {
        let item = state.nodesData.find(i => i.global_id == data.global_id)
        item.name = data.name
    },

    addNode (state, node) {
      // properties
      let properties = []
      for (let name in node.properties) {
        properties.push({
          'property_name': name,
          'property_value': node.properties[name]
        })
      }

      state.nodesData.push({
        'properties': properties,
        'name': node.name,
        'type': node.type,
        'type_id': node.type_id,
        'global_id': node.global_id
      })
    },

    /* 这里得要处理另一个实体不存在。不对呀，不应该在这里处理呀，在submit的时候处理才对
     * 还得处理重边
     * 默认 edge : {name1, name2, type}
     * 默认有向
     * type是个对象
     * 前端没有索引，根本不可能有能力处理稍大一点规模的数据的呀...
     */
    addEdge (state, edge) {
      /*
      const tmpEdge = state.linksData.find(item => {
        if (item.type !== edge.type) {
          return false
        }
        else if (item.source !== edge.name1) {
          return false
        }
        else if (item.target !== edge.name2) {
          return false
        }
        else return true
      })  
      
      console.log(edge + ' from vuex')
      if (tmpEdge === undefined)
        state.linksData.push({
          //'source': edge.name1,
          //'target': edge.name2,
          'global_id': edge.global_id,
          'type': edge.type.name
        })
      */

      // 与getGraph时对links对处理相同
      let l = edge.roles.map((item) => {
        // 处理高阶关系
        let tmp = state.nodesData.find(i => i.global_id == item.inst_id)
        if (tmp == undefined) {
          let tmp_link = state.linksData.find(i => i.global_id == item.inst_id)
          tmp = {
            'global_id': item.inst_id,
            'name': tmp_link.name,
            'type': tmp_link.type,
            'type_id': tmp_link.type_id,
          }
        }

        return {
          'global_id': item.inst_id,
          'role': item.role,
          'entity': tmp
        }
      })

      // 处理一元关系
      //if (l.length == 1) {
      //  l.push(l[0])
      //}

      // 同名关系计数
      if (state.linksDataMap[edge.type] == undefined) {
        state.linksDataMap[edge.type] = 1
      }
      else {
        state.linksDataMap[edge.type]++
      }

      // properties
      let properties = []
      for (let name in edge.properties) {
        properties.push({
          'property_name': name,
          'property_value': edge.properties[name]
        })
      }

      state.linksData.push({
        'global_id': edge.global_id,
        'name': edge.type + '#' + state.linksDataMap[edge.type],
        'type': edge.type,
        'type_id': state.linkTypeList.find(item => item.type == edge.type).global_id,
        'properties': properties,
        'roles': l
      })
    },

    changeProperty (state, data) {
        let global_id = data['inst_id']
        let tmp_node = state.nodesData.find(item => item.global_id == global_id)
        if (tmp_node == undefined) {
          tmp_node = state.linksData.find(item => item.global_id == global_id)
        }

        for (let key in data['properties']) {
          let f = false
          for (let i = 0; i < tmp_node.properties.length; i++)
            if (tmp_node.properties[i].property_name == key) {
              if (data['properties'][key] !== null) {
                tmp_node.properties[i].property_value = data['properties'][key]
              }
              else {
                tmp_node.properties.splice(i, 1)
              }
              
              f = true
              break
            }
          if (!f && data['properties'][key] !== null) {
            tmp_node.properties.push({
              'property_name': key,
              'property_value': data['properties'][key]
            })
          }
        }
    },

    changeTypeProperty (state, data) {
      let global_id = data['type_id']
      let tmp_node = state.nodeTypeList.find(item => item.global_id == global_id)
      if (tmp_node == undefined) {
        tmp_node = state.linkTypeList.find(item => item.global_id == global_id)
      }

      let property_name = data['property_name']
      let symbol_type_id = data['symbol_type_id']
      let symbol_type = state.symbolData[String(symbol_type_id)]
      let f = false
      for (let i = 0; i < tmp_node.properties.length; i++)
        if (tmp_node.properties[i].property_name == property_name) {
          if (data['operation'] == 'addTypeProperty') {
            tmp_node.properties[i].type_id = symbol_type_id
            tmp_node.properties[i].type_name = symbol_type.name
            tmp_node.properties[i].type = symbol_type.type
          }
          else if (data['operation'] == 'deleteTypeProperty') {
            tmp_node.properties.splice(i, 1)
          }

          f = true
          break
        }
      if (!f) {
        if (data['operation'] == 'addTypeProperty') {
          tmp_node.properties.push({
            'property_name': property_name,
            'type_name': symbol_type.name,
            'type_id': symbol_type_id,
            'type': symbol_type.type
          })
        }
      }
    },

    deleteEdge (state, edge) {
      let edge_idx = -1
      for (let i = 0; i < state.linksData.length; i++) {
          if (state.linksData[i] === edge) {
            edge_idx = i
            break
          }
      }
      state.linksData.splice(edge_idx, 1)
    },

    addConstraint (state, data) {
      try {
        const PatientGlobalID = -1  // patient专用id
        let correctFlag = true
        let constraint = {
          precond_item_list: [],
          postcond_item_list: [],
        }
        
        let getStrFromPara = (para) => {  // 获取描述para的str
          if (para.type == 'bool')  {
            console.log('bool')
            console.log(para)
            for(let i = 0; i < para.values.length; i++){
              let item = para.values[i]
              if (item.value == true || item.value == 'true') return 'true'
              else return 'false'
            } 
          }
          else if (para.type == 'number' || para.type == 'int' || para.type == 'float') {
              //return para.number + para.unit
              let tmpStr = ''
              let retStr = '{'
              let firstTag = true
              for(let i = 0; i < para.values.length; i++){
                let item = para.values[i] 
                if (item.name == 'number') {
                  tmpStr = String('数值' + ": " + item.value)
                } else if(item.name == 'unit'){
                  tmpStr = String('单位' + ": " + item.value)
                } else {
                  tmpStr = String(item.name + ": " + item.value)
                }
                if(firstTag){
                  retStr = String(retStr +  tmpStr)
                  firstTag = false
                } else {
                  retStr = String(retStr + ', ' + tmpStr)
                }
              }
              retStr = String(retStr + '}')
              return retStr
          }
          else if (para.type == 'node' || para.type == 'patient') {
            if(para.property_name != 'valid') {
              return para.name + '.' + para.property_name
            } else {
              return para.name + ' '
            }
          }
          else if (para.type == 'string') {
            let tmpStr = '{'
            for(let i = 0; i < para.values.length; i++){
              let item = para.values[i]
              tmpStr = tmpStr + para.values[item].value
            }
            tmpStr = String(tmpStr + '}')
            return tmpStr
          }
        }

        // id
        if (data.id == undefined) {
          constraint.global_id = data.constraint.id
        }
        else {
          constraint.global_id = data.id
        }
        
        var nodesData = state.nodesData
        var linksData = state.linksData
        var nodeTypeList = state.nodeTypeList

        // precondtion_item
        for (let precondItem of data.precond_item_list) {
          let item = {
            // 随机给一个global_id
            global_id : String(Date.now()) + String(Math.floor(Math.random()*10000))
          }
          let preconds = precondItem.precond_list

          // precondition
          item.precondition_para_list = []
          item.precondition_list = []
          let preMap = new Map()
          for (let cond of preconds) {
            let precond = {
              global_id: cond.id,
              opt : cond.opt,
              str : '',
              para1: undefined,
              para2: undefined,
            }
            if (precond.global_id == undefined) {
              precond.global_id = String(Date.now()) + String(Math.floor(Math.random()*10000))
            }

            for (let arg of cond.args) {
              let para = {}
              if (arg.type == "node") {
                let tmp = state.nodesData.find(i => i.global_id == arg.inst_id)
                if (tmp == undefined) {
                  tmp = state.linksData.find(i => i.global_id == arg.inst_id)
                }
                
                para.type = arg.type
                para.global_id = arg.inst_id,
                para.property_name = arg.property
                para.name = tmp.name

                if (preMap.get(arg.inst_id) == undefined) {
                  preMap.set(arg.inst_id, true)
                  item.precondition_para_list.push(arg.inst_id)
                }
              }
              else if (arg.type == "patient") {
                para.type = arg.type
                para.global_id = PatientGlobalID
                para.property_name = arg.property
                para.name = '患者'
              }
              else if (arg.type == 'string') {
                para.type = arg.type
                para.values = arg.values
              }
              else {
                para.type = arg.type
                if (arg.type == "number" || arg.type == "int" || arg.type == "float") {
                  para.values = arg.values
                }
                else if (arg.type == "bool") {
                  para.values = arg.values
                }
              }

              if (arg.arg_location == "left") {
                precond.para1 = para
              }
              else if (arg.arg_location == "right") {
                precond.para2 = para
              }
            }

            if (precond.para1.property_name == "valid") { // 特殊处理bool（其实是特殊处理valid）
              let tmpStr = getStrFromPara(precond.para2)

              let tmpvalid = 'valid'
              let tmpData = nodesData.find(item => item.global_id == precond.para1.global_id)
              if (tmpData != undefined) {
                let tmpType = nodeTypeList.find(item => item.global_id == tmpData.type_id)
                if (tmpType.type == '药物' || tmpType.type == '治疗方式' || tmpType.type == '检查'){
                  tmpvalid = '使用'
                } else if (tmpType.type == '疾病' || tmpType.type == '症状'){
                  tmpvalid = '存在'
                }
              } else if(linksData.find(item => item.global_id == precond.para1.global_id) != undefined){
                tmpvalid = '成立'
              }

              if (tmpStr == 'false') {
                if (tmpvalid != 'valid') {
                  precond.str = getStrFromPara(precond.para1) + '不' + tmpvalid
                } else {
                  precond.str = getStrFromPara(precond.para1) + 'no' + tmpvalid
                }
              } else {
                precond.str = getStrFromPara(precond.para1) + tmpvalid
              }
            } else {
              precond.str = getStrFromPara(precond.para1) + precond.opt + getStrFromPara(precond.para2)
            }

            item.precondition_list.push(precond)
          }

          constraint.precond_item_list.push(item)
        }

        for (let postcondItem of data.postcond_item_list) {
          let item = {
            global_id : String(Date.now()) + String(Math.floor(Math.random()*10000))
          }
          let postconds = postcondItem.postcond_list

          // postcondition
          item.postcondition_para_list = []
          item.postcondition_list = []
          let postMap = new Map()
          for (let cond of postconds) {
            let postcond = {
              global_id: cond.id,
              opt : cond.opt,
              str : '',
              para1: undefined,
              para2: undefined,
            }
            if (postcond.global_id == undefined) {
              postcond.global_id = String(Date.now()) + String(Math.floor(Math.random()*10000))
            }

            for (let arg of cond.args) {
              let para = {}
              if (arg.type == "node") {
                let tmp = state.nodesData.find(i => i.global_id == arg.inst_id)
                if (tmp == undefined) {
                  tmp = state.linksData.find(i => i.global_id == arg.inst_id)
                }
                
                para.type = arg.type
                para.global_id = arg.inst_id,
                para.property_name = arg.property
                para.name = tmp.name

                if (postMap.get(arg.inst_id) == undefined) {
                  postMap.set(arg.inst_id, true)
                  item.postcondition_para_list.push(arg.inst_id)
                }
              }
              else if (arg.type == "patient") {
                para.type = arg.type
                para.global_id = PatientGlobalID
                para.property_name = arg.property
                para.name = '患者'
              }
              else if (arg.type == 'string') {
                para.type = arg.type
                para.values = arg.values
              }
              else {
                para.type = arg.type
                if (arg.type == 'number' || arg.type == "int" || arg.type == "float") {
                  para.values = arg.values
                }
                else if (arg.type == "bool") {
                  para.values = arg.values
                }
              }

              if (arg.arg_location == "left") {
                postcond.para1 = para
              }
              else if (arg.arg_location == "right") {
                postcond.para2 = para
              }
            }

            // 设置描述（str字段）
            if (postcond.para1.property_name == "valid") { // 特殊处理bool（其实是特殊处理valid）
              let tmpStr = getStrFromPara(postcond.para2)
              let tmpvalid = 'valid'

              let tmpData = nodesData.find(item => item.global_id == postcond.para1.global_id)
              if (tmpData != undefined) {
                let tmpType = nodeTypeList.find(item => item.global_id == tmpData.type_id)
                if (tmpType.type == '药物' || tmpType.type == '治疗方式' || tmpType.type == '检查'){
                  tmpvalid = '使用'
                } else if (tmpType.type == '疾病'){
                  tmpvalid = '存在'
                }
              } else if(linksData.find(item => item.global_id == postcond.para1.global_id) != undefined){
                tmpvalid = '成立'
              }

              if (tmpStr == 'false') {
                if (tmpvalid != 'valid') {
                  postcond.str = getStrFromPara(postcond.para1) + '不' + tmpvalid
                } else {
                  postcond.str = getStrFromPara(postcond.para1) + 'no' + tmpvalid
                }
              } else {
                postcond.str = getStrFromPara(postcond.para1) + tmpvalid
              }
            }
            else {
              postcond.str = getStrFromPara(postcond.para1) + postcond.opt + getStrFromPara(postcond.para2)
            }

            item.postcondition_list.push(postcond)
          }

          constraint.postcond_item_list.push(item)
        }

        if (correctFlag) {
          state.constraintData.push(constraint)
        }
      }
      catch(error) {
        return error
      }
    },

    saveGraph (state, data) {
      // 存下来
      state.project_graph[data.project_name] = data.graph

      // 修改当前应该展示的project_name
      state.project_name = data.project_name

      // 修改当前应该展示的graph的信息
      state.nodeTypeList = []
      state.linkTypeList = []
      state.nodesData = []
      state.linksData = []
      state.symbolData = {}
      state.constraintData = []
      state.roleTypeList = []

      //////////////////////
      // 修改nodeTypeList //
      /////////////////////
      for (let entity_type of data.graph.entity_types) {
        let key = entity_type.id
        let type_name = getTypeName(data.graph.entity_types, entity_type.name)
        let type_struct = entity_type.name
        // console.log(type_name)

        // properties
        let properties = []
        for (let name in entity_type.properties) {
          let type_name = entity_type.properties[name].type_name
          let type_id = entity_type.properties[name].type_id
          let type = data.graph.symbol_types.find(i => i.id == type_id).type

          properties.push({
            'property_name': name,
            'type_name': type_name,
            'type_id': type_id,
            'type': type
          })
        }

        let newNodeType = {
            'global_id': key,
            'name': type_name,
            'type': type_name,
            'properties': properties
        }
        
        // 处理复杂类型：交和并
        try {
          type_struct = JSON.parse(type_struct)
          if (typeof(type_struct) == 'string') {
            // 0表示基本类型，1表示交，2表示并
            newNodeType['isComplex'] = SimpleType

            state.nodeTypeList.push(newNodeType)
          }
          else {
            let isComplex = IntersectionType
            if (type_struct.operator == "or") isComplex = UnionType
              else isComplex = IntersectionType
            
            // 0表示基本类型，1表示交，2表示并
            newNodeType['isComplex'] = isComplex
            newNodeType['struct'] = type_struct
            
            state.nodeTypeList.push(newNodeType)
          }
        }
        catch (err) {
          newNodeType['isComplex'] = SimpleType
          
          state.nodeTypeList.push(newNodeType)
        }
      }

      //////////////////////
      // 修改roleTypeList //
      /////////////////////
      if (data.graph.role_types != undefined) {
        for (let roleType of data.graph.role_types) {
          let id = roleType.id
          let type_ids = roleType.type_ids
          let name = ''

          // get name
          for (let ids of type_ids) {
            let tmp = data.graph.entity_types.find(i => i.id == ids)
            if (tmp == undefined)
              tmp = data.graph.relation_types.find(i => i.id == ids)
            if (name != '') name += UnionOpt
            name += tmp.name
          }

          state.roleTypeList.push({
            'global_id': id,
            'type_ids': type_ids,
            'name': name,
            'type': name,
          })
        }
      }

      //////////////////////
      // 修改linkTypeList //
      /////////////////////
      for (let relation_type of data.graph.relation_types) {
        let key = relation_type.id
        // 处理roles
        let l = []
        for (let r in relation_type.roles) {
          let type_name = ''
          let type_id = relation_type.roles[r].type_id
          let tmp = state.roleTypeList.find(i => i.global_id == type_id)
          if (tmp != undefined) {
            // 关系的承担者是实体或关系
            type_name = tmp.name
          }
          else {
            type_name = getTypeName(data.graph.entity_types, relation_type.roles[r].type_name)
          }

          ////////////////////////////////////////////////////////////////////////
          // 注意：role的type_id有三种：实体，关系，实体并关系（实体并关系在roleType中） //
          ///////////////////////////////////////////////////////////////////////
          l.push({
            'name': r, // 扮演的角色名
            'type': type_name, // 角色的类型
            'global_id':  String(type_id),
            'roles': relation_type.roles[r] // 存了所有东西
          })
        }

        // 处理properties
        let properties = []
        for (let name in relation_type.properties) {
          let type_name = relation_type.properties[name].type_name
          let type_id = relation_type.properties[name].type_id
          let type = data.graph.symbol_types.find(i => i.id == type_id).type

          properties.push({
            'property_name': name,
            'type_name': type_name,
            'type_id': type_id,
            'type': type
          })
        }

        state.linkTypeList.push({
          'global_id': key,
          'name': relation_type.name,
          'type': relation_type.name,
          'properties': properties,
          'entity': l
        })
      }

      ///////////////////
      // 修改nodesData //
      //////////////////
      // 重置计数器
      state.nodesDataMap = {}
      for (let i = 0; i < state.nodesData.length; i++) {
        state.nodesData[state.nodesData[i].name] = 0
      }

      for (let entity of data.graph.entities) {
        let key = entity.id

        // type_id & type_name
        let type_id = entity.type_id
        let type_name = ''
        if (type_id == undefined) {
          type_name = getTypeName(data.graph.entity_types, entity.type)
          type_id = state.nodeTypeList.find(item => item.name == type_name).global_id
        }
        else {
          type_name = state.nodeTypeList.find(item => item.global_id == type_id).name
        }

        // properties
        let properties = []
        for (let name in entity.properties) {
          properties.push({
            'property_name': name,
            'property_value': entity.properties[name]
          })
        }

        let tmp = {
          'global_id': key,
          'name': entity.name,
          'type': type_name,
          'type_id': type_id,
          'properties': properties //属性list
        }

        state.nodesData.push(tmp)
      }

      ///////////////////
      // 修改linksData //
      //////////////////
      // 同名关系计数
      state.linksDataMap = {}
      for (let i = 0; i < state.linkTypeList.length; i++) {
        state.linksDataMap[state.linkTypeList[i].name] = 0
      }

      for (let relation of data.graph.relations) {
        //let key = relation.id
        let type_id = relation.type_id
        let type_name = state.linkTypeList.find(item => item.global_id == type_id).name

        if (state.linksDataMap[type_name] == undefined) {
          state.linksDataMap[type_name] = 1
        }
        else {
          state.linksDataMap[type_name]++
        }
        relation['name'] = type_name + '#' + state.linksDataMap[type_name]
      }

      for (let relation of data.graph.relations) {
        let key = relation.id
        // console.log("relation: " + key)
        //console.log(key)
        // 处理roles
        let l = []
        l = relation.roles.map((item) => {
          // 处理高阶关系，'entity'可以是关系，记录下所有的东西
          let tmp = state.nodesData.find(i => i.global_id == item.inst_id)
          if (tmp == undefined) {
            let role_relation = data.graph.relations.find(i => i.id == item.inst_id)
            tmp = {
              'global_id': String(item.inst_id),
              'name': role_relation.name,
              'type': role_relation.type,  // 这个字段有问题
              'type_id': role_relation.type_id,
            }
          }

          return {
            // string
            'role': item.role,
            'global_id': item.inst_id,
            // nodeType
            'entity': tmp
          }
        })
        
        // 处理一元关系
        //if (l.length === 1) {
        //  l.push(l[0])
        //}

        // properties
        let properties = []
        for (let name in relation.properties) {
          properties.push({
            'property_name': name,
            'property_value': relation.properties[name]
          })
        }

        state.linksData.push({
          'global_id': key,
          'type': state.linkTypeList.find(item => item.global_id == relation.type_id).name,
          'type_id': relation.type_id,
          'name': relation.name,
          'roles': l,
          'properties': properties
        })
      }

      ////////////////
      // 处理symbol //
      ///////////////
      state.symbolData = {}
      for (let symbol_type of data.graph.symbol_types) {
        state.symbolData[String(symbol_type.id)] = {}
        for (let key in symbol_type) {
          if (key != 'id') state.symbolData[String(symbol_type.id)][key] = symbol_type[key]
        }
      }
      console.log("symbol")
      console.log(state.symbolData)

      /////////////
      // 处理约束 //
      ////////////
      state.constraintData = []
      //console.log('get graph')
      console.log(data.graph)
      if (data.graph.constraints != undefined) {
        for (let item of data.graph.constraints) {
          this.commit('addConstraint', item)
        }
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
