// 导入element plus
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

// 全局注册element icons
import * as ElIconModules from '@element-plus/icons'
import { transElIconName } from './util/utils'  

// 导入vuex
import Vuex from 'vuex';

// axios
import axios from 'axios'
import VueAxios from 'vue-axios';

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

console.log = () => {}

const app = createApp(App)
app.use(Vuex)
app.use(store)
app.use(router)
app.use(ElementPlus)
app.use(VueAxios, axios)
app.mixin({
    methods: {
        // 禁止输入自定义的非法字符
        validateForbidChar: function (value) {
            //console.log(value)
            let tmpVal = value.replace(/[`~!@$%^&*()_+=<>?:"{}|,./;'\\[\]·~！@￥%……&*（）——+={}|《》？：“”【】、；‘’，。、]/g, "")
                            .replace(/\s/g, "")
            //console.log(tmpVal)
            return tmpVal
        },

        // 禁止输入自定义的非法字符（用于数值单位）
        validateUnitForbidChar: function (value) {
            //console.log(value)
            let tmpVal = value.replace(/[`~!#@$%^&*()_\-+=<>?:"{}|,.;'\\[\]·~！@￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]/g, "")
                            .replace(/\s/g, "")
            //console.log(tmpVal)
            return tmpVal
        }
    }
})

// 统一注册el-icon图标
for(let iconName in ElIconModules){
    app.component(transElIconName(iconName),ElIconModules[iconName])
}

app.mount('#app')
