import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../components/KGList.vue'
import store from '../store/index.js'
import { ElMessage } from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

const routes = [
  {
    path: '/', // 这个是默认路径！
    redirect: '/loginForm'
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/loginForm',
    name: 'LoginForm',
    component: () => import('../components/LoginForm.vue')
  },
  {
    path: '/registerForm',
    name: 'RegisterForm',
    component: () => import('../components/RegisterForm.vue')
  },
  {
    path: '/instruction',
    name: 'Instruction',
  },
  /* 这部分放到navigation里面了
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  { 
    path: '/KG',
    name: "KG",
    meta: { requiresAuth: true },
    component: () => import('../components/KG.vue')
  },
  {
    path: '/aboutUs',
    name: 'AboutUs',
    meta: { requiresAuth: true },
    component: () => import('../components/AboutUs.vue')
  },
  */
  {
    path: '/navigation',
    name: 'Navigation',
    meta: { requiresAuth: true },
    component: () => import('../components/Navigation.vue'),
    children: [
      {
        path: '',
        component: Home,
        meta: { requiresAuth: true },
      },
      {
        path: 'home',
        name: 'Home',
        component: Home,
        meta: { requiresAuth: true },
      },
      {
        path: 'KG',
        name: "KG",
        meta: { requiresAuth: true },
        component: () => import('../components/KG/KG.vue')
      },
      {
        path: 'KGModel',
        name: 'KGModel',
        meta: { requiresAuth: true },
        component: () => import('../components/KGModel/KGModel.vue')
      },
      {
        path: 'aboutUs',
        name: 'AboutUs',
        meta: { requiresAuth: true },
        component: () => import('../components/AboutUs.vue')
      }
    ]
  },
  {
    path: '/*',
    redirect: '/loginForm',
  }
  
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  //console.log(from.name, '->', to.name);
  //console.log(to.matched); //这玩意儿返回了个list，应该是所有匹配上的路由对象
  //console.log(to.matched.some(record => record.meta.requiresAuth))
  console.log(`store_test: ${store.state.testValue}`)
  let token = localStorage.getItem('Authorization');

  if (to.name === 'Instruction') {
    console.log('?')
    next(false) 
  }
  if (to.name === 'LoginForm') { // 如果已经登录了就不再跳转到登陆界面了
    if (token === null || token === '') {
      next();
    }
    else {
      next('/navigation');
    }
  }
  else if (to.matched.some(record => record.meta.requiresAuth)) {
    //console.log('hello')
    if (token === null || token === '') {
      next('/loginForm');
    }
    else if (to.name === 'KG' || to.name === 'KGModel') {
      if (store.state.project_name === '') {
        ElMessage({
          type: 'info',
          message: '请先选择一个知识图谱'
        })
        next('/navigation/home')
      }
      else {
        next();
      }
    }
    else {
      next()
    }
  }
  else {
    next();
  }
})

export default router
