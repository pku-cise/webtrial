//cnost server_target = 'http://122.9.11.16:8080'
const server_target = 'http://162.105.89.129:6790'
//const server_target = 'http://127.0.0.1:8080'

module.exports = {
    devServer: {
        open: true, //是否自动弹出浏览器页面
        host: "0.0.0.0", 
        port: '80',
        https: false,
        hotOnly: false,
        proxy: {
            '/api': {
                target: server_target,
                ws: true,  //代理websockets
                secure: false,
                changeOrigin: true
            },
            '/project': {
                target: server_target,
                ws: true,  //代理websockets
                secure: false,
                changeOrigin: true
            },
            '/model': {
                target: server_target,
                ws: true,  //代理websockets
                secure: false,
                changeOrigin: true
            },
            '/inst': {
                target: server_target,
                ws: true,  //代理websockets
                secure: false,
                changeOrigin: true
            },
        }
    }
}